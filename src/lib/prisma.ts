import { PrismaClient } from "@prisma/client";

const globalPrisma = global as unknown as {
  prisma: PrismaClient;
};

// This prevent creating new instance of prisma client when developing because hot reloading
export const prisma =
  globalPrisma.prisma ||
  new PrismaClient({
    log: ["query"],
  });

if (process.env.NODE_ENV !== "production") {
  globalPrisma.prisma = prisma;
}
