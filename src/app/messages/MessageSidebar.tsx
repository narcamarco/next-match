"use client";

import { useMessageStore } from "@/hooks/useMessageStore";
import { Chip } from "@nextui-org/react";
import clsx from "clsx";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import React, { useState } from "react";
import { GoInbox } from "react-icons/go";
import { MdOutlineOutbox } from "react-icons/md";

const MessageSidebar = () => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const { unreadCount } = useMessageStore((state) => {
    return {
      unreadCount: state.unreadCount,
    };
  });

  const [selected, setSelected] = useState<string>(
    searchParams.get("container") || "inbox"
  );

  const items = [
    {
      key: "inbox",
      label: "Inbox",
      icon: <GoInbox size={24} />,
      chip: true,
    },
    {
      key: "outbox",
      label: "Outbox",
      icon: <MdOutlineOutbox size={24} />,
      chip: false,
    },
  ];

  const handleSelect = (key: string) => {
    setSelected(key);
    const params = new URLSearchParams();
    params.set("container", key);

    router.replace(`${pathname}?${params}`);
  };

  return (
    <div className="flex flex-col shadow-md rounded-lg cursor-pointer">
      {items.map(({ key, icon: Icon, label, chip }) => {
        return (
          <div
            key={key}
            className={clsx(
              "flex flex-row items-center, rounded-t-lg gap-2 p-3",
              {
                "text-secondary font-semibold": selected === key,
                "text-black hover:text-secondary/70": selected !== key,
              }
            )}
            onClick={() => handleSelect(key)}
          >
            {Icon}
            <div className="flex justify-between flex-grow">
              <span>{label}</span>

              {chip && <Chip>{unreadCount}</Chip>}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default MessageSidebar;
