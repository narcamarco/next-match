import React, { Suspense } from "react";
import MessageSidebar from "./MessageSidebar";
import { getMessagesByContainer } from "../actions/messageAction";
import MessageTable from "./MessageTable";

const MessagesPage = async ({
  searchParams,
}: {
  searchParams: { container: string };
}) => {
  const { messages, nextCursor } = await getMessagesByContainer(
    searchParams.container
  );
  console.log({ messages });

  return (
    <div className="grid grid-cols-12 gap-5 h-[80vh] mt-10">
      <div className="col-span-2">
        <MessageSidebar />
      </div>

      <div className="col-span-10">
        <Suspense>
          <MessageTable initialMessages={messages} nextCursor={nextCursor} />
        </Suspense>
      </div>
    </div>
  );
};

export default MessagesPage;
