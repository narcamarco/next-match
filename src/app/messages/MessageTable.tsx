"use client";

import { MessageDto } from "@/types";
import {
  Avatar,
  Button,
  Card,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  getKeyValue,
} from "@nextui-org/react";
import { useRouter, useSearchParams } from "next/navigation";
import React, { Key, useCallback, useState } from "react";
import { AiFillDelete } from "react-icons/ai";
import { deleteMessage } from "../actions/messageAction";
import { truncateString } from "@/lib/util";
import PresenceAvatar from "@/components/PresenceAvatar";
import MessageTableCell from "./MessageTableCell";
import { useMessages } from "@/hooks/useMessages";

type Props = {
  initialMessages: MessageDto[];
  nextCursor?: string;
};

const MessageTable = ({ initialMessages, nextCursor }: Props) => {
  const {
    selectRow,
    isDeleting,
    isOutbox,
    columns,
    deleteMessage,
    messages,
    loadMore,
    loadingMore,
    hasMore,
  } = useMessages(initialMessages, nextCursor);

  return (
    <div className="flex flex-col h-[80vh]">
      <Card className="">
        <Table
          aria-label="Table with messages"
          selectionMode="single"
          onRowAction={(key) => selectRow(key)}
          shadow="none"
          className="flex flex-col gap-3 h-[80vh] overflow-auto"
        >
          <TableHeader columns={columns}>
            {(column) => (
              <TableColumn
                key={column.key}
                width={column.key === "text" ? "50%" : undefined}
              >
                {column.label}
              </TableColumn>
            )}
          </TableHeader>

          <TableBody
            items={messages}
            emptyContent="No messages for this container"
          >
            {(item) => {
              return (
                <TableRow key={item.id} className="cursor-pointer">
                  {(columnKey) => (
                    <TableCell
                      className={`${
                        !item.dateRead && !isOutbox ? "font-semibold" : ""
                      }`}
                    >
                      {/* <div
                      className={`${
                        !item.dateRead && !isOutbox ? "font-semibold" : ""
                      }`}
                    >
                      {getKeyValue(item, columnKey)}
                    </div> */}

                      <MessageTableCell
                        item={item}
                        columnKey={columnKey as string}
                        isOutbox={isOutbox}
                        deleteMessage={deleteMessage}
                        isDeleting={
                          isDeleting.loading && isDeleting.id === item.id
                        }
                      />
                    </TableCell>
                  )}
                </TableRow>
              );
            }}
          </TableBody>
        </Table>

        <div className="sticky bottom-0 pb-3 mr-3 text-right">
          <Button
            color="secondary"
            isLoading={loadingMore}
            isDisabled={!hasMore}
            onClick={loadMore}
          >
            {hasMore ? "Load More" : "No More Messages"}
          </Button>
        </div>
      </Card>
    </div>
  );
};

export default MessageTable;
