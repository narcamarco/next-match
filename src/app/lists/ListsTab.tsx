"use client";

import { Spinner, Tab, Tabs } from "@nextui-org/react";
import { Member } from "@prisma/client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import React, { Key, useTransition } from "react";
import MemberCard from "../members/MemberCard";
import LoadingComponent from "@/components/LoadingComponent";

type Props = {
  members: Member[];
  likeIds: string[];
};
const ListsTab = ({ members, likeIds }: Props) => {
  const searchParams = useSearchParams();
  const router = useRouter();
  const pathname = usePathname();
  const [isPending, startTransition] = useTransition();

  const tabs = [
    {
      id: "source",
      label: "Members I have liked",
    },
    {
      id: "target",
      label: "Members that liked me",
    },
    {
      id: "mutual",
      label: "Mutual liked",
    },
  ];

  const handleTabChange = (key: Key) => {
    startTransition(() => {
      const params = new URLSearchParams(searchParams);
      params.set("type", key.toString());
      router.replace(`${pathname}?${params.toString()}`);
    });
  };

  return (
    <div className="flex w-full flex-col mt-10 gap-5">
      <div className="flex items-center">
        <Tabs
          aria-label="Like tabs"
          color="secondary"
          onSelectionChange={(key) => handleTabChange(key)}
        >
          {tabs.map((item) => {
            return <Tab key={item.id} title={item.label} />;
          })}
        </Tabs>

        {isPending && (
          <Spinner color="secondary" className="self-center ml-3" />
        )}
      </div>

      {tabs.map((item) => {
        const isSelected = searchParams.get("type") === item.id;

        return isSelected ? (
          <div>
            {members.length > 0 ? (
              <div className="mt-10 grid grid-cols-2 md:grid-cols-3 xl:grid-cols-6 gap-8">
                {members.map((member) => {
                  return (
                    <MemberCard
                      key={member.id}
                      member={member}
                      likeIds={likeIds}
                    />
                  );
                })}
              </div>
            ) : (
              <div>No Members for this filter</div>
            )}
          </div>
        ) : null;
      })}
      {/* 
        {(item) => {
          return (
            <Tab key={item.id} title={item.label}>
              {isPending ? (
                <LoadingComponent />
              ) : (
                <>
                  {members.length > 0 ? (
                    <div className="mt-10 grid grid-cols-2 md:grid-cols-3 xl:grid-cols-6 gap-8">
                      {members.map((member) => {
                        return (
                          <MemberCard
                            key={member.id}
                            member={member}
                            likeIds={likeIds}
                          />
                        );
                      })}
                    </div>
                  ) : (
                    <div>No Members for this filter</div>
                  )}
                </>
              )}
            </Tab>
          );
        }}
      </Tabs> */}
    </div>
  );
};

export default ListsTab;
