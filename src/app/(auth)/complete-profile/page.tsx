import React from "react";
import CompleteProfileForm from "./CompleteProfileForm";

const CompleteProfilePage = () => {
  return <CompleteProfileForm />;
};

export default CompleteProfilePage;
