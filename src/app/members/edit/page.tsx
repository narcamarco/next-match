import { CardBody, CardHeader, Divider } from "@nextui-org/react";
import React from "react";
import EditForm from "./EditForm";
import { getAuthUserId } from "@/app/actions/authAction";
import { getMemberByUserId } from "@/app/actions/memberAction";
import { notFound } from "next/navigation";

const EditPage = async () => {
  const userId = await getAuthUserId();

  const member = await getMemberByUserId(userId);

  if (!member) {
    return notFound();
  }

  return (
    <>
      <CardHeader className="text-2xl font-semibold text-secondary">
        Edit Profile
      </CardHeader>
      <Divider />
      <CardBody>
        <EditForm member={member} />
      </CardBody>
    </>
  );
};

export default EditPage;
