import Link from "next/link";
import { getMembers } from "../actions/memberAction";
import MemberCard from "./MemberCard";
import { fetchCurrentUserLikeIds } from "../actions/likeAction";
import PaginationComponent from "@/components/PaginationComponent";
import { GetMemberParams, UserFilters } from "@/types";
import EmptyState from "@/components/EmptyState";

const MembersPage = async ({
  searchParams,
}: {
  searchParams: GetMemberParams;
}) => {
  const { items: members, totalCount } = await getMembers(searchParams);
  const likeIds = await fetchCurrentUserLikeIds();

  return (
    <>
      {!members || members.length === 0 ? (
        <EmptyState />
      ) : (
        <>
          <div className="mt-10 grid grid-cols-2 md:grid-cols-3 xl:grid-cols-6 gap-8">
            {members &&
              members.map((member) => {
                return (
                  <MemberCard
                    key={member.id}
                    member={member}
                    likeIds={likeIds}
                  />
                );
              })}
          </div>

          <PaginationComponent totalCount={totalCount} />
        </>
      )}
    </>
  );
};

export default MembersPage;
