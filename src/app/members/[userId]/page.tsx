import { getMemberByUserId } from "@/app/actions/memberAction";
import CardInnerWrapper from "@/components/CardInnerWrapper";
import { CardBody, CardHeader, Divider } from "@nextui-org/react";
import { notFound } from "next/navigation";
import React from "react";

const MemberDetailedPage = async ({
  params,
}: {
  params: {
    userId: string;
  };
}) => {
  const member = await getMemberByUserId(params.userId);

  if (!member) {
    return notFound();
  }

  return (
    <CardInnerWrapper header="Profile" body={<div>{member.description}</div>} />
  );
};

export default MemberDetailedPage;
