import { getMemberPhotosByUserId } from "@/app/actions/memberAction";
import MemberPhotos from "@/components/MemberPhotos";
import { CardBody, CardHeader, Divider, Image } from "@nextui-org/react";
import React from "react";

const PhotosPage = async ({ params }: { params: { userId: string } }) => {
  const photos = await getMemberPhotosByUserId(params.userId);
  return (
    <>
      <CardHeader className="text-2xl font-semibold text-secondary">
        Photos
      </CardHeader>
      <Divider />
      <CardBody>
        {/* <div className="grid grid-cols-5 gap-3">
          {photos &&
            photos.map((photo) => {
              return (
                <div className="" key={photo.id}>
                  <Image
                    width={300}
                    height={300}
                    src={photo.url}
                    alt="Image of Member"
                    className="object-cover aspect-square"
                  />
                </div>
              );
            })}
        </div> */}

        <MemberPhotos photos={photos} />
      </CardBody>
    </>
  );
};

export default PhotosPage;
