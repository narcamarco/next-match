import CardInnerWrapper from "@/components/CardInnerWrapper";
import React from "react";
import ChatForm from "./ChatForm";
import { getMessageThread } from "@/app/actions/messageAction";
import { getAuthUserId } from "@/app/actions/authAction";
import MessageList from "./MessageList";
import { createChatId } from "@/lib/util";

const ChatPage = async ({ params }: { params: { userId: string } }) => {
  const messages = await getMessageThread(params.userId);
  console.log({ messages });
  const userId = await getAuthUserId();
  const chatId = createChatId(userId, params.userId);

  return (
    <CardInnerWrapper
      header="Chat"
      body={
        <MessageList
          initialMessages={messages}
          currentUserId={userId}
          chatId={chatId}
        />
      }
      footer={
        <div className="w-full">
          <ChatForm />
        </div>
      }
    />
  );
};

export default ChatPage;
