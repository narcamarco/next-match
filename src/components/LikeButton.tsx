"use client";

import { toggleLikeMember } from "@/app/actions/likeAction";
import { useRouter } from "next/navigation";
import React from "react";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";
import { PiSpinnerGap } from "react-icons/pi";

type Props = {
  toggleLike: () => void;
  hasLiked: boolean;
  loading: boolean;
};

const LikeButton = ({ toggleLike, loading, hasLiked }: Props) => {
  return (
    <>
      {!loading ? (
        <div
          className="relative transition cursor-pointer hover:opacity-80"
          onClick={toggleLike}
        >
          <AiOutlineHeart
            size={28}
            className="fill-white absolute -top-[2px] -right-[2px]"
          />

          <AiFillHeart
            size={24}
            className={hasLiked ? "fill-rose-500" : "fill-neutral-500/70"}
          />
        </div>
      ) : (
        <PiSpinnerGap size={32} className="fill-white animate-spin" />
      )}
    </>
  );
};

export default LikeButton;
