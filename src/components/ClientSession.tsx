"use client";

import { useSession } from "next-auth/react";
import React from "react";

const ClientSession = () => {
  const session = useSession();
  return (
    <div className="bg-blue-50 p-50 rounded-xl shadow-md w-1/2 overflow-auto">
      <h3 className="text-2xl font-semibold">Client Session data:</h3>
      {session ? (
        <div>
          <pre>{JSON.stringify(session, null, 2)}</pre>
          {/* <form
              action={async () => {
                "use server";

                await signOut();
              }}
            >
              <Button
                type="submit"
                color="primary"
                variant="bordered"
                startContent={<FaRegSmile size={20} />}
              >
                Sign Out
              </Button>
            </form> */}
        </div>
      ) : (
        <div className="">Not Signed</div>
      )}
    </div>
  );
};

export default ClientSession;
