import { MessageDto } from "@/types";
import { create } from "zustand";
import { devtools } from "zustand/middleware";

type MessageState = {
  messages: MessageDto[];
  unreadCount: number;
  add: (message: MessageDto) => void;
  remove: (id: string) => void;
  set: (messages: MessageDto[]) => void;
  updatedUnreadCount: (amount: number) => void;
  resetMessages: () => void;
};

export const useMessageStore = create<MessageState>()(
  devtools(
    (set) => {
      return {
        messages: [],
        unreadCount: 0,
        add: (message: MessageDto) =>
          set((state) => ({ messages: [message, ...state.messages] })),
        remove: (id: string) => {
          return set((state) => {
            return {
              messages: state.messages.filter((message) => message.id !== id),
            };
          });
        },
        set: (messages: MessageDto[]) =>
          set((state) => {
            const map = new Map(
              [...state.messages, ...messages].map((m) => [m.id, m])
            );

            console.log({ map });

            const uniqueMessages = Array.from(map.values());

            return {
              messages: uniqueMessages,
            };
          }),
        updatedUnreadCount: (amount: number) =>
          set((state) => ({ unreadCount: state.unreadCount + amount })),
        resetMessages: () => set({ messages: [] }),
      };
    },
    {
      name: "messageStore",
    }
  )
);
