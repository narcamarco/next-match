import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FaFemale, FaMale } from "react-icons/fa";
import useFilterStore from "./useFilterStore";
import { ChangeEvent, useEffect, useTransition } from "react";
import { Selection } from "@nextui-org/react";
import usePaginationStore from "./usePaginationStore";

export const useFilters = () => {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const router = useRouter();

  const [isPending, startTransition] = useTransition();

  const { filters, setFilters } = useFilterStore();
  const { pageNumber, pageSize, setPage } = usePaginationStore((state) => {
    return {
      pageNumber: state.pagination.pageNumber,
      pageSize: state.pagination.pageSize,
      setPage: state.setPage,
    };
  });

  const { gender, ageRange, orderBy, withPhoto } = filters;

  const orderByList = [
    {
      label: "Last Active",
      value: "updated",
    },
    {
      label: "Newest Members",
      value: "created",
    },
  ];

  const genderList = [
    {
      value: "male",
      icon: FaMale,
    },
    {
      value: "female",
      icon: FaFemale,
    },
  ];

  //   const selectedGender = searchParams.get("gender")?.split(",") || [
  //     "male",
  //     "female",
  //   ];

  const handleAgeSelect = (value: number[]) => {
    // const params = new URLSearchParams(searchParams);
    // params.set("ageRange", value.join(","));

    setFilters("ageRange", value);
  };

  const handleOrderSelect = (value: Selection) => {
    if (value instanceof Set) {
      setFilters("orderBy", value.values().next().value);
      //   const params = new URLSearchParams(searchParams);
      //   params.set("orderBy", value.values().next().value);
    }
  };

  const handleGenderSelect = (value: string) => {
    if (gender.includes(value)) {
      setFilters(
        "gender",
        gender.filter((g) => g !== value)
      );
    } else {
      setFilters("gender", [...gender, value]);
    }
    // const params = new URLSearchParams(searchParams);

    // if (selectedGender.includes(value)) {
    //   params.set(
    //     "gender",
    //     selectedGender.filter((g) => g !== value).toString()
    //   );
    // } else {
    //   params.set("gender", [...selectedGender, value].toString());
    // }
  };

  const handleWithPhotoToggle = (e: ChangeEvent<HTMLInputElement>) => {
    setFilters("withPhoto", e.target.checked);
  };

  useEffect(() => {
    startTransition(() => {
      const searchParams = new URLSearchParams();

      if (gender) {
        searchParams.set("gender", gender.join(","));
      }

      if (ageRange) {
        searchParams.set("ageRange", ageRange.toString());
      }

      if (orderBy) {
        searchParams.set("orderBy", orderBy);
      }

      if (pageSize) {
        searchParams.set("pageSize", pageSize.toString());
      }

      if (pageNumber) {
        searchParams.set("pageNumber", pageNumber.toString());
      }

      searchParams.set("withPhoto", withPhoto.toString());

      router.replace(`${pathname}?${searchParams}`);
    });
  }, [
    ageRange,
    orderBy,
    gender,
    router,
    pathname,
    pageSize,
    pageNumber,
    withPhoto,
  ]);

  useEffect(() => {
    if (gender || ageRange || orderBy || withPhoto) {
      setPage(1);
    }
  }, [gender, ageRange, orderBy, setPage, withPhoto]);

  return {
    orderByList,
    genderList,
    selectAge: handleAgeSelect,
    selectGender: handleGenderSelect,
    selectOrder: handleOrderSelect,
    filters,
    isPending,
    selectWithPhoto: handleWithPhotoToggle,
  };
};
